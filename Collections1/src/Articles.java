import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Articles extends Collect{
    private String Author;
    private String Title;
    private int nrofparaph;
    private String Paragraph;
    private int priority;
    private int importance;

    public Articles(String author, String title, int nrofparaph, String paragraph, int priority, int importance) {
        Author = author;
        Title = title;
        this.nrofparaph = nrofparaph;
        Paragraph = paragraph;
        this.priority = priority;
        this.importance = importance;
    }

    public String getAuthor() {
        return Author;
    }

    public String getTitle() {
        return Title;
    }

    public int getNrofparaph() {
        return nrofparaph;
    }

    public String getParagraph() {
        return Paragraph;
    }

    public int getPriority() {
        return priority;
    }

    public int getImportance() {
        return importance;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setNrofparaph(int nrofparaph) {
        this.nrofparaph = nrofparaph;
    }

    public void setParagraph(String paragraph) {
        Paragraph = paragraph;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setImportance(int importance) {
        this.importance = importance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Articles)) return false;
        Articles articles = (Articles) o;
        return getNrofparaph() == articles.getNrofparaph() &&
                getPriority() == articles.getPriority() &&
                getImportance() == articles.getImportance() &&
                getAuthor().equals(articles.getAuthor()) &&
                getTitle().equals(articles.getTitle()) &&
                getParagraph().equals(articles.getParagraph());
    }

    @Override
    public List<Articles> Sortbypriority(Object p) {
        return super.Sortbypriority(p);
    }

    @Override
    public int compareTo(Object o) {
        return super.compareTo(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
